<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Configuration Cookies
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */
/**
 * @var $cookie['prefix'] = 'myprefix'; // prefixo do cookie (opcional)
 * @var $cookie['time']   = '36000'; // tempo de duração do cookie (opcional), se ele não for definido o cookie irá durar enquanto o browser permanecer aberto
 * @var $cookie['path']   = '/'; // o caminho do servido a onde o cookie estára disponivel
 * @var $cookie['domain'] = 'www2.example.com'; // subdominio para qual o coockie estará disponivel (opcional)
 * @var $cookie['secure'] = false; // indica que o cookie só poderar se transmitido por conexão segura(HTTPS)
 */

$cookie['prefix'] = '';
$cookie['time']   = '';
$cookie['path']   = '/';
$cookie['domain'] = '';
$cookie['secure'] = false;
