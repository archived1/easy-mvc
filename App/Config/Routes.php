<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Configuration Routes
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */


/**
 * Configurações de rotas
 * @example $routes['path'] = pathClass@method ;
 */
$routes[''] = 'Home';