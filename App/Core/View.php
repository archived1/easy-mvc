<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  View
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


class View
{
    /**
     * @var Output $output armazena a instancia da classe Cora\Output
     */
    public $output;

    /**
     * Construtor da View
     *
     * @access public
     * @see instancia a class Core\Output( ) e atribui a @var instance $output
     * @return void
     *
     */
    public function __construct()
    {
        $this->output = new \App\Core\Output;
    }
    
    /**
     * Carrega Views
     *
     * @access public
     * @param string $view view a ser carregada
     * @return void
     * Obs.: as views a serem carregadas devem estar no diretorio Views/
     *
     */
    public function load($view)
    {
        $file = getFile('Views',$view);
        if(file_exists($file))
        {
            include $file;
        }
        else
        {
            $header = 'View não encontrada';
            $mensage = "Não foi possivel encontra a view {$view}";
            new Exception('fatalError',$mensage,$header);
        }
    }
}