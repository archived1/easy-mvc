<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Controller
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


abstract class Controller
{
    /**
     * @var Views $views armazena a instancia da classe Core\View
     * @var Models $model armazena a instancia da classe filha de model Models\ClassName
     * @var Session $session armazena a instancia da classe Core\Session
     * @var Cookies $cookies armazena a instancia da classe Core\Cookies
     */
    protected $view;
    protected $model;
    protected $session;
    protected $cookies;


    /**
     * Construtor do Controller
     *
     * @access public
     * @see instancia a class Core\View( ) e atribui a @var instance $view
     * @see instancia a class Core\Sessio( ) e atribui a @var instance $session
     * instancia a class Session
     * @return void
     *
     */
    public function __construct()
    {
        $this->view    = new \App\Core\View();
        $this->session = new \App\Core\Session();
        $this->cookie  = new \App\Core\Cookies();
    }

    /**
     * Define metodo a ser implementado pela classe filha
     *
     * @access public
     * @return void
     *
     */
    public abstract function index();

    /**
     * Model filho a ser carregado
     *
     * @access public
     * @param string $model model filha a ser instanciada, e atribuida a @var instance $model
     * @return void
     *
     */
    public function loadModel($model)
    {
        if(file_exists(getFile('Models',$model)))
        {
            $class = "\App\Models\\$model";
            $this->model = new $class;
        }
    }

    /**
     * Lança exception para chamadas de metodos não existentes
     *
     * @access public
     * @param string $name nome do metodo chamado
     * @param string $aguments argumentos passado no metodo não existente
     * @return void
     *
     */
    public function __call($name, $aguments)
    {
        $controller = get_class($this);
        $header     = 'Metodo não encontrado';
        $menssage   = "Error: o metodo {$name} do Controller {$controller} não foi encontrado.";
        new Exception('showError',$menssage,$header);
    }
    
}