<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Model
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


class Model
{
    /**
     * @var \PDO $db armazena a instância da class \PDO
     * @var string $table armazena o nome da tabela a ser manipulada
     */
    protected $db;
    protected $table;

    /**
     * Construtor de Model
     *
     * @access public
     */
    public function __construct()
    {
        try {
            // carregando arquivo de configuração
            require getFile('Config','Database');

            $db['driver'] = isset($db['driver']) ? $db['driver'] : '';
            $db['host']   = isset($db['host']) ? $db['host'] : '';
            $db['name']   = isset($db['name']) ? $db['name'] : '';
            $db['user']   = isset($db['user']) ? $db['user'] : '';
            $db['pass']   = isset($db['pass']) ? $db['pass'] : '';

            $this->db = new \PDO("{$db['driver']}:host={$db['host']};dbname={$db['name']}", $db['user'], $db['pass']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->table = null;
        } catch(\PDOException $e) {
            /**
             * Lança uma exception caso a conexão com o banco de dados falhe
             */
            $header = 'Falha ao conectar ao banco de dados';
            $mensage = 'Não foi possivel estabelecer uma conexão com o banco de dados'.$e->getMessage();
            new Exception('fatalError', $mensage, $header);
        }
    }

    /**
     * Definir tabela para manipulação
     *
     * @param string $name nome da tabela
     * @return Database $this
     */
    public function table($name) {
        $this->table = $name;
        return $this;
    }

    /**
     * Executar uma busca na tabela definida
     *
     * @param array $wheres filtro de busca
     * @example $wheres['colunaNome'] = valor;
     * @param string $select colunas a serem retornadas
     * @param integer $rows numero de resultados a ser retornados
     * @return array
     */
    public function get(Array $wheres = null, $select = null, $rows = null) {
        // prepara queries select
        $select = !empty($select) ? $select : '*';

        // prepara queries where
        if(!empty($wheres)) {
            $wheres = implode(', ', array_map(function($value,$field){
                return "{$field}='{$value}'";
            },$wheres, array_keys($wheres)));

            $wheres =  'WHERE '. $wheres;
        }
        else
            $wheres = '';

        // prepara queries limit
        $limit = !empty($rows) ? "LIMIT {$rows}" : '';

        // constrói string SQL
        $sql = "select {$select} from {$this->table} {$wheres} {$limit}";
        $query = $this->db->query($sql);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     *   Executar uma busca na tabela definida e retorna somente o primeiro resultado
     *
     * @param array $wheres filtro de busca
     * @example $wheres['colunaNome'] = valor;
     * @param string $select colunas a serem retornadas
     * @return bool|array resultado
     */
    public function get_first(Array $wheres = null, $select = null) {
        $result = $this->get($wheres,$select,1);
        return !empty($result) ? $result[0] : false;
    }

    /**
     * Inserir linhas na tabela definida
     *
     * @param array $fields
     * @example $fields['colunaNome'] = valor;
     * @return bool|int retorna id da linha inserida
     */
    public function insert(Array $fields) {
        // prepara queries values
        $_fields = implode(", ", array_keys($fields));
        $_values = implode(", ", array_map(function($value) {
            return ":{$value}";
        }, array_keys($fields)));

        // constrói string SQL
        $sql = "INSERT INTO {$this->table}({$_fields}) VALUES({$_values})";
        $stmt = $this->db->prepare($sql);

        // seta bind statement do pdo
        foreach ($fields as $field => $value)
            $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);

        if($stmt->execute())
            return $this->db->lastInsertId();

        return false;
    }

    /**
     * Atualizar linhas na tabela definida
     *
     * @param array $fields linhas a serem alteradas
     * @example $fields['colunaNome'] = valor;
     * @param array $wheres filtro de alteração
     * @example $wheres['colunaNome'] = valor;
     * @return bool|int
     */
    public function update(Array $fields, Array $wheres) {
        // prepara queries set
        $_fields = implode(', ', array_map(function($field){
            return "{$field}=:{$field}";
        }, array_keys($fields)));
        $_fields = "SET {$_fields}";

        // prepara queries where
        $_wheres = implode(', ', array_map(function($field){
            return "{$field}=:{$field}";
        },array_keys($wheres)));
        $_wheres =  'WHERE '. $_wheres;

        // constrói string SQL
        $sql = "UPDATE {$this->table} {$_fields} {$_wheres}";
        $stmt = $this->db->prepare($sql);

        // seta bind statement do pdo
        foreach ($fields as $field => $value)
            $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);
        foreach ($wheres as $field => $value)
            $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);

        if($stmt->execute())
            return $stmt->rowCount();

        return false;
    }

    /**
     * Remover linhas na tabela definida
     *
     * @param array $wheres filtro para remoção
     * @example $wheres['colunaNome'] = valor;
     * @return bool|int
     */
    public function remove(Array $wheres) {
        // prepara queries where
        $_wheres = implode(', ', array_map(function($field){
            return "{$field}=:{$field}";
        },array_keys($wheres)));

        $_wheres =  'WHERE '. $_wheres;

        // constrói string SQL
        $sql = "DELETE FROM {$this->table} {$_wheres}";
        $stmt = $this->db->prepare($sql);

        // seta bind statement do pdo
        foreach ($wheres as $field => $value)
            $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);

        if($stmt->execute())
            return $stmt->rowCount();

        return false;
    }

    /**
     * Conta os resultados de uma consulta
     *
     * @param array $wheres
     * @return int
     */
    public function count_result(Array $wheres = null) {
        // prepara queries where
        $wheres = implode(', ', array_map(function($value,$field){
            return "{$field}='{$value}'";
        },$wheres, array_keys($wheres)));
        $wheres =  'WHERE '. $wheres;

        // constrói string SQL
        $sql = "select * from {$this->table} {$wheres}";
        $stmt = $this->db->prepare($sql);

        $stmt->execute();
        return $stmt->rowCount();
    }
}