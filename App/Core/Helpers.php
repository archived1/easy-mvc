<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Helpers
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */


/**
 * Retorna o caminho raiz até um diretorio
 *
 * @param string $dir diretorio a pegar caminho raiz
 * @return string $path: o caminho até o diretorio
 *
 */
function getPath($dir)
{
    $path = 'App'.DS;
    if (file_exists($path.$dir))
        return $path.$dir.DS ;

    throw new ErrorException("Error: path `{$path}` not found!");
}

/**
 * Retorna o caminho raiz até um arquivo
 *
 * @param string $dir diretorio do arquivo
 * @param string $file arquivo a pegar caminho raiz
 * @return string $path: o caminho até o arquivo
 *
 */
function getFile($dir,$file)
{
    $basepath  = 'App'.DS;
    $dir  .= DS;
    $file .= '.php';
    $path = $basepath.$dir.$file;

    if(file_exists($path))
        return $path;

    throw new ErrorException("Error: file `{$path}` not found!");
}

/**
 * Retorna url base mais path url
 *
 * @param string $url (opcional) path url, caso não seja passado recebe valor null
 * @return string $url retorna url base mais path url caso tenha sido passada.
 *
 */
function url($url = NULL)
{
    $protocolo = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';
    $urlBase   = $protocolo . $_SERVER['HTTP_HOST'];
    $urlBase  .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
    $url       = $urlBase.$url;
    return $url;
}