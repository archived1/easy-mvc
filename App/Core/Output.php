<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Output
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


class Output
{
    /**
     * @var HtmlElement $tag armazena a instancia da classe Core\Output\HtmlElement
     * @var Table $table armazena a instancia da classe Core\Output\Table
     */
    public $tag;
    public $table;

    /**
     * Construtor de Output
     *
     * @access public
     * @see instancia a class Core\Output\HtmlElement( ) e atribui a @var instance $tag
     * @see instancia a class Core\Output\Table( ) e atribui a @var instance $table
     * @return void
     *
     */
    public function __construct()
    {
        $this->tag = new HtmlElementFactory();
        $this->table = new TableFactory();
    }

}


class HtmlElementFactory
{
    public function create($tag)
    {
        return new HtmlElment($tag);
    }
}

class TableFactory
{
    public function create()
    {
        return new Table();
    }
}

class HtmlElement
{
    /**
     * @var string $tag tag a ser criada
     * @var string $attributes atributos da tag
     * @var array $self_closers lista de tag que fecha em si mesmas
     */
    private $tag;
    private $attributes;
    private $self_closers;

    public function __construct($tag)
    {
        $this->tag = $tag;
        $this->self_closers = array('input','img','hr','br','meta','link');
    }

    /**
     * Captura atributo
     *
     * @access public
     * @param string $attribute atributo a ser capturado
     * @return string $attribute;
     *
     */
    public function get($attribute)
    {
        if(isset($attribute[$attribute]))
            return $this->attributes[$attribute];
    }

    /**
     * Seta atributo
     *
     * @access public
     * @param string $attribute atributo a ser setado
     * @return void
     *
     */
    public  function set($attribute,$value = NULL)
    {
        if(!is_array($attribute))
        {
            $this->attributes[$attribute] = $value;
        }
        else
        {
            $this->attributes = array_merge($this->attributes,$attribute);
        }
    }

    /**
     * Cremove atributos
     *
     * @access public
     * @param string $attribute atributo a ser removido
     * @return void;
     *
     */
    public function remove($attribute)
    {
        if(isset($attribute[$attribute]))
            unset($this->attributes[$attribute]);
    }

    /**
     * Insere tag dentro de outra
     *
     * @access public
     * @param object $object tag a ser inserida
     * @return void
     *
     */
    function inject($object)
    {
        if(!isset($this->attributes['text']))
            $this->attributes['text'] = NULL;

        if(@get_class($object) == __class__)
        {
            $this->attributes['text'].= $object->build();
        }
    }

    /**
     * Constrói tags html
     *
     * @access public
     * @return string $build retorna string com codigo html
     *
     */
    private function build()
    {

        //start
        $build = '<'.$this->tag;

        //add atributes
        if(count($this->attributes))
        {
            foreach ($this->attributes as $atribute => $value)
            {   $value = (isset($value) ? '="'.$value.'"' : NULL);

                if($atribute != 'text') { $build .= ' '.$atribute.$value; }
            }
        }

        //closing
        if(!in_array($this->tag,$this->self_closers))
        {
            if(!isset($this->attributes['text']))
                $this->attributes['text'] = NULL;

            $build .= '>'.$this->attributes['text'].'</'.$this->tag.'>';
        }
        else{
            $build .= '/>';
        }
        return $build;

    }
    /**
     * limpa os atributos da tag
     *
     * @access public
     * @return void
     *
     */
    public function clear()
    {
        $this->attributes = array();
    }

    /**
     * Gera a saida do codigo html
     *
     * @access public
     * @param boolean $return (opcional) por padrão o valor e false a onde ele gera a saida do codigo html,
     * caso seja passado true ele ira retorna o codigo html
     * @return string $return codigo html
     */
    public function output($return = false)
    {
        if($return)
            return $this->build();
        else
            echo $this->build();
    }

}

class Table
{
    /**
     * @var array $table atributos da tag table
     * @var array $col colunas da tabela
     * @var array $row linhas da tabela
     */
    private $table;
    public $col;
    private $row;

    /**
     * Adiciona atributos a tag table
     *
     * @access public
     * @param string $attribute atributo a ser adicionado a tag table,
     * @param string $value valor do atributo a ser adicionado a tag table
     * @return void
     *
     */
    public function addAttribTable($attribute,$value)
    {
        $this->table[$attribute] = $value;
    }

    /**
     * Remove atributos da tag table
     *
     * @access public
     * @param string $attribute atributo a ser removido da tag table,
     * @return Boolean true se a operação for bem sucedida
     *
     */
    public function removeAttribTable($attribute)
    {
        if(array_key_exists($attribute,$this->table))
        {
            unset($this->table[$attribute]);
            return true;
        }
    }

    /**
     * Adiciona colunas a tabela
     *
     * @access public
     * @param string $ColName nome da coluna a ser adicionado a tabela
     * @param string $attribute(opcional)  atributo a ser adicionado a tag th
     * @param string $value(opcional) valor do atributo a ser adicionado a tag th
     * @return void
     *
     */
    public function addCol($colName, $attribute = NULL, $value = NULL)
    {
        if($attribute)
            $this->col[$colName][$attribute] = $value;
        else
            $this->col[$colName] = NULL;
    }

    /**
     * Remove Colunas da Tabela
     *
     * @access public
     * @param string $ColName nome da coluna a ser removida da tabela
     * @return boolean true se a operação for bem sucedida
     *
     */
    public function removeCol($colName)
    {
        if(array_key_exists($colName,$this->col))
        {
            unset($this->col[$colName]);
            return true;
        }
    }

    /**
     * Adiciona atributo a coluna da tabela
     *
     * @access public
     * @param string $ColName nome da coluna a receber o atributo
     * @param string $attribute  atributo a ser adicionado a tag th
     * @param string $value valor do atributo a ser adicionado a tag th
     * @return boolean true se a operação for bem sucedida
     *
     */
    public function addAttribCol($colName,$attribute,$value)
    {
        if(array_key_exists($colName,$this->col))
        {
            $this->col[$colName][$attribute] = $value;
            return true;
        }
    }

    /**
     * Remove atributo da coluna da tabela
     *
     * @access public
     * @param string $ColName nome da coluna a remover o atributo
     * @param string $attribute  atributo a ser removido da tag th
     * @return boolean true se a operação for bem sucedida
     *
     */
    public function removeAttribCol($colName,$attribute)
    {
        if(array_key_exists($attribute,$this->col[$colName]))
        {
            unset($this->col[$colName][$attribute]);
            return true;
        }

    }

    /**
     * Pega lista com todas as colunas da tabela
     *
     * @access public
     * @return array com colunas
     *
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * Adiciona linha na tabela
     *
     * @access public
     * @param mixed $row conteúdo da linha da tabela
     * @param mixed $indice indice (opcional) da linha da tabela
     * @param string $attribute (opcional) atributo a ser adicionado a linha
     * @param string $value (opcional) valor do atributo a ser adicionado a linha
     * @return void
     *
     */
    public function addRow($row, $indice = NULL, $attribute = NULL, $value = NULL)
    {
        $arrayRow = ($attribute ? array($row, $attribute => $value): array($row,array()));

        if($indice || $indice === 0)
            $this->row[$indice] = $arrayRow;
        else
            $this->row[] = $arrayRow;
    }

    /**
     * remove linha da tabela
     *
     * @access public
     * @param mixed $indice indice da linha da tabela
     * @return boolean true se a operação for bem sucedida
     *
     */
    public function removeRow($indice)
    {
        if(array_key_exists($indice,$this->row))
        {
            unset($this->row[$indice]);
            return true;
        }
    }

    /**
     * Adiciona atributo a linha da tabela
     *
     * @access public
     * @param string $indice indece da coluna a receber o atributo
     * @param string $attribute  atributo a ser adicionado a linha
     * @param string $value valor do atributo a ser adicionado a linha
     * @return boolean true se a operação for bem sucedida
     *
     */
    public function addAttribRow($indice,$attribute,$value)
    {
        if(array_key_exists($indice,$this->row))
        {
            $this->row[$indice][$attribute] = $value;

        }
    }

    /**
     * Remove atributo da linha da tabela
     *
     * @access public
     * @param string $indice indece da linha a remover o atributo
     * @param string $attribute  atributo a ser removido da linha
     * @return boolean true se a operação for bem sucedida
     *
     */
    public function removeAttribRow($indice,$attribute)
    {
        if(array_key_exists($attribute,$this->row[$indice]))
        {
            unset($this->row[$indice][$attribute]);
            return true;
        }
    }

    /**
     * pega uma linha da tabela
     *
     * @access public
     * @param string $indice indece da linha a ser capturada
     * @return array com elementos da linha
     *
     */
    public function getRow($indice)
    {
        if(array_key_exists($indice,$this->row))
            return $this->row[$indice];
    }

    /**
     * Constroi tag table
     *
     * @access public
     * @return string com tag table
     *
     */
    private function addTagTable()
    {
        $table = new HtmlElement('table');
        if(!empty($this->table))
        {
            foreach ($this->table as $attribute => $value)
            {
                $table->set($attribute,$value);
            }
        }
        return $table;
    }

    /**
     * Constroi tag th
     *
     * @access public
     * @return string com tag th
     *
     */
    private function addTagCols()
    {
        $tr = new HtmlElement('tr');
        foreach ($this->col as $colName => $values)
        {
            $th = new HtmlElement('th');

            if(count($values))
            {
                foreach ($values as $attribute => $value)
                {
                    $th->set($attribute,$value);
                }
            }

            $th->set('text',$colName);
            $tr->inject($th);

        }
        return $tr;
    }

    /**
     * Constroi tag tr
     *
     * @access public
     * @return string com tag tr
     *
     */
    private function addTagRows()
    {
        foreach ( $this->row as $indice => $row )
        {   $tr = new HtmlElement('tr');
            $tr->set('data-id',$indice);
            foreach ($row as $attribute => $value)
            {
                if(is_array($value))
                {

                    foreach ($value as $cell)
                    {
                        $td = new HtmlElement('td');
                        $td->set('text',$cell);
                        $tr->inject($td);
                    }
                }
                else
                {
                    $tr->set($attribute,$value);

                }

            }
            $rows[] = $tr;
        }
        return $rows;
    }

    /**
     * Constroi codigo html da tabela
     *
     * @access public
     * @return string com codigo html
     *
     */
    public function builder()
    {
        $table = $this->addTagTable();
        $col   = $this->addTagCols();
        $rows  = $this->addTagRows();

        $table->inject($col);

        foreach ($rows as $row)
        {
            $table->inject($row);
        }

        $table->output();

    }

    /**
     * Gera a saida do codigo html
     *
     * @access public
     * @param boolean $return (opcional) por padrão o valor e false a onde ele gera a saida do codigo html,
     * caso seja passado true ele ira retorna o codigo html
     * @return string $return codigo html
     */
    public function output($return = false)
    {
        if($return)
            return $this->build();
        else
            echo $this->build();
    }
}
