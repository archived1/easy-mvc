<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Cookies
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */
namespace App\Core;


class Cookies
{
    /**
     * @var string $prefix armazena prefixo do cookie
     * @var integer $time armazena tempo para o cookie expirar
     * @var string $path armazena a path a onde o cookie estará disponivel
     * @var string $domain armazena o subdominio para qual o cookie estará disponivel
     * @var boolean $secure indica que o cookie só podera ser transmitido sob uma conexão segura HTTPS
     */
    private $prefix;
    private $time;
    private $path;
    private $domain;
    private $secure;

    public function __construct()
    {
        include getFile('Config','Cookies');
        $this->prefix = isset($cookie['prefix']) ? $cookie['prefix'] : null;
        $this->time   = isset($cookie['time']) ? $cookie['time'] : 0 ;
        $this->path   = isset($cookie['path']) ? $cookie['path'] : null;
        $this->domain = isset($cookie['domain']) ? $cookie['domain'] : null;
        $this->secure = isset($cookie['secure']) ? $cookie['secure'] : null;
    }

    /**
     * Cria um cookie
     *
     * @access public
     * @param string $cookieName nome do cookie a ser criado
     * @param string $cookieValue valor do cookie a ser criado
     * @return void || true se ação for bem sucedida
     *
     */
    public function create($cookieName, $cookieValue, $cookieTime = null, $cookiePath = null, $cookieDomain = null, $cookieSecure = null)
    {
        $cookieTime   = ($cookieTime ? cookieTime : $this->time);
        $cookiePath   = ($cookiePath ? $cookiePath : $this->path);
        $cookieDomain = ($cookieDomain ? $cookieDomain : $this->domain);
        $cookieSecure = ($cookieSecure ? $cookieSecure : $this->secure);

        if(setcookie($this->prefix.$cookieName, $cookieValue, $cookieTime, $cookiePath, $cookieDomain, $cookieSecure))
            return true;
    }

    /**
     * Captura um cookie
     *
     * @access public
     * @param string $cookieName nome do cookie a ser capturado
     * @return void || $_COOKIE retorna o cookie
     *
     */
    public function getCookie($cookieName)
    {
        if($_COOKIE[$this->prefix.$cookieName])
            return $_COOKIE[$this->prefix.$cookieName];
    }

    /**
     * Destroi um cookie especifico
     *
     * @access public
     * @param string $cookieName nome do cookie a ser destruido
     * @return void || true se ação for bem sucedida
     *
     */
    public function destroy($cookieName)
    {
        if(isset($_COOKIE[$this->prefix.$cookieName])) {
            unset($_COOKIE[$this->prefix.$cookieName]);
            return true;
        }
    }

    /**
     * Destroi todos os cookies
     *
     * @access public
     * @return void
     *
     */
    public  function destroyAll()
    {
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
    }

}