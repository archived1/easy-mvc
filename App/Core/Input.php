<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Input
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


class Input
{
    /**
     * Captura parametros passado pelo metodo get
     *
     * @access public
     * @param string $value nome do paramentro get a ser capturado
     * @param string $default valor padrão a ser retornado caso índice get não seja encontrado
     * @return array || string
     *
     */   
    public static function get($name = null, $default = null)
    {
        return empty($name) ? $_GET : (isset($_GET[$name]) ? $_GET[$name] : $default);
    }

    /**
     * Captura parametros passado pelo metodo post
     *
     * @access public
     * @param string $name nome do índice post a ser capturado
     * @param string $default valor padrão a ser retornado caso índice post não seja encontrado
     * @return array || string
     *
     */
    public static function post($name = null, $default = null)
    {
        return empty($name) ? $_POST : (isset($_POST[$name]) ? $_POST[$name] : $default);
    }
}