<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Execption
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


class Exception
{
    /**
     * Construtor da Exception
     *
     * @access public
     * @param string $type tipo da exception a ser lançada
     * @param string $mensage a mensagem de error a ser exibida
     * @param string $header cabeçalho do error a ser exibido
     * @param string $title titulo do error a ser exibido
     *
     */
    public function __construct($type, $mensage, $header = NULL, $title = NULL)
    {
        if($type == 'showError')
            $this->showError($mensage, $header, $title);
        else
            $this->$type($mensage, $header);
    }

    /**
     * Lança uma Exception de página não encontrada
     *
     * @access public
     * @param string $mensage a mensagem de error a ser exibida
     * @return void
     *
     */
    public function error404($mensage)
    {
        $title  = 'Error 404';
        $header = 'Pagína não encontrada';
        include getFile('Views','Error');
        exit;
    }

    /**
     * Lança uma Exception de fatal error
     *
     * @access public
     * @param string $mensage a mensagem de error a ser exibida
     * @param string $header cabeçalho de error
     * @return void
     *
     */
    public function fatalError($mensage,$header)
    {
        $title = 'Fatal Error';
        include getFile('Views','Error');
        exit;
    }

    /**
     * Lança uma Exception de error
     *
     * @access public
     * @param string $mensage a mensagem de error a ser exibida
     * @param string $header cabeçalho de error
     * @param string $title titlo do error
     * @return void
     *
     */
    public function showError($mensage, $header, $title)
    {
        include getFile('Views','Error');
        exit;
    }
}