<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Router
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


class Router
{
    /**
     * @var array $routes armazena array ccm controllers e actions de rotas
     */
    protected $routes;

    /**
     * Construtor de Router
     *
     * @access public
     * @var array $routes inicia o atributo com array de rotas do arquivo @uses
     * @uses App\Config\Routes.php
     */
    public function __construct()
    {
        include getFile('Config', 'Routes');
        $this->routes = isset($routes) ? $routes : array();
    }

    /**
     * Inicia o controle de rotas
     *
     * @access public
     * @return void
     *
     */
    public function startRoutes()
    {
        $routeCurrent = $this->getRoute();
        $route = explode('@', $this->searchRoute($routeCurrent));

        /**
         *  Constroi o controller da rota.
         */
        if(!empty($route[0])) {
            $class = "\\App\\Controllers\\{$route[0]}";
            $action  = isset($route[1]) ? $route[1] : 'index';
            $controller = new $class;
            call_user_func([$controller,$action]);
        }
        else
            new Exception('error404',"A página {$routeCurrent} não foi encontrada, verificar endereço.");
    }

    /**
     * Pequisa se uma rota exite
     *
     * @access private
     * @param string $route rota a ser procurada
     * @return array $route se a rota for encontrada retorna array com controller e action da rota
     *
     */
    private function searchRoute($route)
    {
        foreach ($this->routes as $name => $path) {
            if ($name == $route)
                return $path;
        }
    }

    /**
     * Captura a rota da url
     *
     * @access private
     * @return string $route retona rota extraida da url
     *
     */
    private function getRoute()
    {
        // Captura url
        $getUrl = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        // Define path base url
        $pathUrl = parse_url(url(), PHP_URL_PATH);

        // Extrai rota da url
        if(strlen($pathUrl) != 1)
            $route =  str_replace($pathUrl,NULL,$getUrl);
        else
            $route = substr($getUrl,1);

        // Remove barra depois da rota
        $lastDS = explode('/', $getUrl);
        $count = count($lastDS) -1;
        if (empty($lastDS[$count]))
            $route = substr($route, 0, -1);

        return strtolower($route);
    }
}