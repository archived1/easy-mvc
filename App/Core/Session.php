<?php
/**
 * Easy MVC
 *
 * @package     Easy MVC
 * @subpackage  Session
 * @author      D. Carvalho,  <victordanilo_cs@live.com>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

namespace App\Core;


class Session
{

    /**
     * Cria uma sessão
     *
     * @access public
     * @param mixed $data dados a serem armazenado na sessão
     * @param  string $name nome da sessão
     * @return void
     *
     */
    public function create($data,$name)
    {
        session_start();
        $_SESSION[$name] = $data;
    }

    /**
     * Pega uma sessão 
     *
     * @access public
     * @param string $session nome da sessão a ser capturada
     * @return mixed $_SESSION[$session] retorna a sessão.
     *
     */
    public function getSession($session)
    {
        session_start();
        if(isset($_SESSION[$session]))
            return $_SESSION[$session];
    }

    /**
     * Destroi uma sessão especifica
     *
     * @access public
     * @param string $session nome da sessão a ser destruida
     * @return true se ação for bem sucedida
     *
     */
    public function destroy($session)
    {
        session_start();
        if(isset($_SESSION[$session])) {
            unset($_SESSION[$session]);
            return true;
        }
    }

    /**
     * Destroi todas sessões
     *
     * @access public
     * @return void
     *
     */
    public function destroyAll()
    {
        session_start();
        session_destroy();
    }
    
}