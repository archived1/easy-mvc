<?php
/**
 * Easy MVC
 *
 * @package     Core
 * @subpackage  AutoLoad
 * @author      Erik Figueiredo <falecom@erikfigueiredo.com.br>
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link        http://blog.erikfigueiredo.com.br/
 *
 */

namespace App\Core;


class AutoLoad
{
    /**
     * @var string $ext armazena a extenção do arquivo a ser carregado
     * @var string $prefix armazena o prefixo do arquivo a ser carregado
     * @var string $sufix armazena o sufixo do arquivo a ser carregado
     */
    private $ext;
    private $prefix;
    private $sufix;

    /**
     * Define o caminho local até a raiz do script
     *
     * @access public
     * @param string $path caminho completo até o script
     * @return void
     *
     */
    public function setPath($path)
    {
        set_include_path($path);
    }

    /**
     * Define a extensão do arquivo a ser carregado
     *
     * @access public
     * @param string $ext a extensão sem o ponto
     * @return void
     *
     */
    public function setExt($ext)
    {
        $this->ext='.'.$ext;
    }

    /**
     * Define se havera algo a se colocar antes do nome do arquivo
     *
     * @access public
     * @param string $prefix o que vai antes do nome do arquivo
     * @return void
     *
     */
    public function setPrefix($prefix)
    {
        $this->prefix=$prefix;
    }

    /**
     * Define se havera algo a se colocar após o nome do arquivo
     *
     * @access public
     * @param string $sufix o que vai após o nome do arquivo
     * @return void
     *
     */
    public function setSufix($sufix)
    {
        $this->sufix=$sufix;
    }

    /**
     * Transforma a classe em caminho até o arquivo correspondente
     *
     * @access private
     * @param string $className caminho completo até o script
     * @return string $fileName: o caminho até o arquivo da classe
     *
     */
    private function setFilename($className)
    {
        $className = ltrim($className, '\\');
        $fileName  = '';
        $namespace = '';
        if ($lastNsPos = strrpos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $className = $this->prefix.$className.$this->sufix;
            $fileName  = str_replace('\\', DS, $namespace) . DS;
        }
        $fileName .= str_replace('_', DS, $className) . $this->ext;
        return $fileName;
    }

    /**
     * Carrega arquivos do diretorio App
     *
     * @access public
     * @param string $className a classe a se carregar
     * @return void
     *
     */
    public function load($className)
    {
        $fileName=$this->setFilename($className);
        $fileName=get_include_path().DS.$fileName;

        if (is_readable($fileName)) {
            include $fileName;
        }
        else {
            $mensage = "Não foi possivel encontrar class {$fileName}";
            $this->loadError($mensage);
        }
    }

    /**
     * Lança uma Exception de error de load de class
     *
     * @access private
     * @param string $mensage mensage de error a se exibida
     * @return void
     *
     */
    private function loadError($mensage)
    {
        $title  = 'Error AutoLoad';
        $header = 'Class não encontrada';
        new \App\Core\Exception('showError',$mensage,$header,$title);
    }

}