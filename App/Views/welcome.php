<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Easy MVC</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }
        .full-height {
            height: 100vh;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
        .position-ref {
            position: relative;
        }
        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }
        .content {
            text-align: center;
        }
        .title {
            font-size: 84px;
        }
        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div id="box" class="title m-b-md"></div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/typeit@6.0.3/dist/typeit.min.js"></script>
<script>
    window.onload = function() {
        new TypeIt('#box', {
            speed: 150,
            breakLines: false,
            waitUntilVisible: true,
            loop: true
        })
        .type('Welcome')
        .pause(1500)
        .delete(7)
        .type('The Easy MVC is Simple')
        .pause(1000)
        .delete(6)
        .type('Fast')
        .pause(1000)
        .delete(4)
        .type('the right way')
        .break()
        .type('to start your project')
        .go();
    };
</script>
</body>
</html>