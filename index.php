<?php
/**
 * Easy MVC
 *
 *@package     Easy MVC
 *@category    Min-Framework MVC
 *@version     0.1
 *@author      D. Carvalho <victordanilo_cs@live.com>
 *@license     http://opensource.org/licenses/gpl-license.php, GNU Public License
 *
 */


/**
 * Configuração de Encode Charset
 *
 */
ini_set('default_charset', 'UTF-8');
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
header('Content-Type: text/html; charset=UTF-8');

/**
 * Define Ambiente da Aplicação.
 *
 * reporting de error de cada ambiente:
 *
 * development   All
 * testing       Fatal Error
 * production    Not reporting
 *
 */
define('ENVIRONMENT', 'development');

if (defined('ENVIRONMENT'))
{
    switch (ENVIRONMENT)
    {
        case 'development':
            error_reporting(E_ALL);
            ini_set('display_errors', -1);
            break;
        case 'testing':
            error_reporting(E_ERROR);
            ini_set('display_errors', -1);
        case 'production':
            error_reporting(0);
            break;
        default:
            exit('O Ambiente da aplicação não foi definido corretamente!');
    }
}

/**
 * Configuração de Timezone
 */
$timezone = 'America/Sao_Paulo';
date_default_timezone_set($timezone);

/**
 * Define o caminho raiz da aplicação
 */
define('BASEPATH',dirname(__FILE__));

/**
 * Define uma abreviação do Directory Separator
 */
define('DS',DIRECTORY_SEPARATOR);

/**
 * Carrega boostrap da aplicação
 */
include 'App'.DS.'Core'.DS.'Helpers.php';

/**
 * Inicia o AutoLoad
 *
 * @method setPath($path) seta a path a base do script.
 * @method setExt($ext) seta a extenção dos arquivos a serem carregados
 * @return void
 *
 */
require 'App'.DS.'Core'.DS.'AutoLoad.php';
$autoLoad = new App\Core\AutoLoad();
$autoLoad->setPath(BASEPATH);
$autoLoad->setExt('php');

/**
 * Registra o metodo load na pilha de autoload da SPL
 */
spl_autoload_register(array($autoLoad, 'load'));

/**
 * Inicia as rotas
 *
 * @method startRoutes( ) inicia o controle de rotas
 * @return void
 *
 */
use App\Core\Router;
$router = new Router();
$router->startRoutes();